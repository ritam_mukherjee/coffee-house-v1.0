package org.ig.coffeehouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*@SpringBootApplication*/
public class CoffeDayStarter {
    public static void main(String[] args) {
        SpringApplication.run(CoffeDayStarter.class,args);
    }
}
