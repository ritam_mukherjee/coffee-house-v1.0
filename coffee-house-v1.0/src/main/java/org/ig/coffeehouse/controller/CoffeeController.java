package org.ig.coffeehouse.controller;


import org.ig.coffeehouse.model.Coffee;
import org.ig.coffeehouse.model.Customer;
import org.ig.coffeehouse.services.CoffeeService;
import org.ig.coffeehouse.services.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class CoffeeController {


    @Autowired
    private CoffeeService coffeeService;

    @Autowired
    private InventoryService inventoryService;

    @RequestMapping("/coffeeOrderPlace")
    public String CoffeeOrderLaunch(){

        System.out.println("coffee order place");
        ModelAndView modelAndView=new ModelAndView("CoffeeOrderPlace");
        modelAndView.addObject("msg","Robs's Coffee Shop");
        return "CoffeeOrderPlace";
    }


    /*Similar method can be expose where update will happen based on coffee type*/
    @RequestMapping(value="/coffeeOrder",method = RequestMethod.PUT)
    public String updateCoffeeInventory(@RequestParam(value="coffee_id", required=false, defaultValue="C11") String coffee_id,
                                      @RequestParam(value="serving", required=false, defaultValue="1") int serving) {

        System.out.println("The update is going to happen for:"+coffee_id);
        System.out.println("The total serving will be provided:"+serving);
        coffeeService.updateSellingAvailability(coffee_id,serving);
        inventoryService.updateSellingAvailability_cofffe_Id(coffee_id,serving);
        return "Update Successfully";
    }
    /*Similar method can be expose where update will happen based on coffee type*/
    @RequestMapping(value="/coffeeType",method = RequestMethod.GET)
    public String GetCoffeeInventory(@RequestParam(value="coffee_id", required=false, defaultValue="C11") String coffee_id
                                       ) {

        System.out.println("The update is going to happen for:"+coffee_id);

       String coffee_type=coffeeService.getCoffee(coffee_id);
        return coffee_type;
    };



    @RequestMapping("/coffeeOrderServe")
    public ModelAndView coffeeServe(@RequestParam(value="coffeeName",required = false,defaultValue = "latte")String coffeeName,
                                   @RequestParam(value="servings",required = false,defaultValue = "1")int servings){
        Coffee coffee=new Coffee("11",coffeeName,"strong",servings);
        coffee.setCoffee_type(coffeeName);
        coffee.setAvailable_serving(servings);

        ModelAndView modelAndView=new ModelAndView("CoffeeOrderSuccess");
        modelAndView.addObject("coffee",coffee);
        return modelAndView;
    }

    @RequestMapping("/coffees")
    public List<Coffee> getAllCoffees(Model model) {
        /*Set value when List is empty*/
        if(coffeeService.getAllCoffees().size()==0)
            {
                coffeeService.addCoffee(new Coffee("C11", "Cappucino", "Lite", 50));
                coffeeService.addCoffee(new Coffee("C22", "Latte", "Strong", 100));
            }
            model.addAttribute("coffees",coffeeService.getAllCoffees());
        return coffeeService.getAllCoffees();
    }

    @RequestMapping(value="/addCoffee",method = RequestMethod.POST)
    public ModelAndView addCoffee(@RequestBody List<Coffee>coffees) {

        coffees.forEach(coffee -> coffeeService.addCoffee(coffee));
        return new ModelAndView(new RedirectView("forward:coffees",true));
    }

}
