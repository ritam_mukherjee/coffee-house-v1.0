package org.ig.coffeehouse.controller;

import javafx.application.Application;
import org.ig.coffeehouse.model.Customer;
import org.ig.coffeehouse.repository.ICustomerRepository;
import org.ig.coffeehouse.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;


@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;


    @RequestMapping("/customers")
    public List<Customer> getAllCustomers() {
        customerService.addCustomer(new Customer("11",33,"RAM"));
        return customerService.getAllCustomers();
    }



    @RequestMapping(value="/addCustomer",method = RequestMethod.POST)
    public ModelAndView addCustomer(@RequestBody List<Customer>customers) {

        System.out.println("BEFORE:"+customers.size());

        customers.forEach(customer -> customerService.addCustomer(customer));
        System.out.println("AFTER:"+customers.size());
       return new ModelAndView(new RedirectView("forward:customers",true));
    }
}
