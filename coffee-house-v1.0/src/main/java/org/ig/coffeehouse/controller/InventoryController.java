package org.ig.coffeehouse.controller;

import org.ig.coffeehouse.custombeans.Report;
import org.ig.coffeehouse.model.Coffee;
import org.ig.coffeehouse.model.Inventory;
import org.ig.coffeehouse.repository.IInventoryRepository;
import org.ig.coffeehouse.services.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;


   // List<Inventory> inventories=getAllInventory();

    @RequestMapping("/inventories")
    public List<Inventory> getAllInventory() {

        if(inventoryService.getAllInventoris().size()==0) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
/*            System.out.println("DATE:>" + dateFormat.format(new Date()));*/
            inventoryService.addInventory(new Inventory("I11", "C11", 200, 50, dateFormat.format(new Date())));
        }
        return inventoryService.getAllInventoris();
    }

    @RequestMapping(value="/addInventory" +
            "",method = RequestMethod.POST)
    public ModelAndView addInventory(@RequestBody List<Inventory> inventories) {
        System.out.println("BEFORE:"+inventories.size());
        inventories.forEach(inventory -> inventoryService.addInventory(inventory));
        System.out.println("AFTER:"+inventories.size());
        return new ModelAndView(new RedirectView("forward:coffees",true));
    }

    @RequestMapping(value="/updateInventory",method = RequestMethod.PUT)
    public String updateItemInventory(@RequestParam(value="itemId", required=false, defaultValue="I11") String itemId,
                                      @RequestParam(value="serving", required=false, defaultValue="1") int serving) {

        System.out.println(itemId);
        System.out.println(serving);
        inventoryService.updateSellingAvailability(itemId,serving);
        return "Update Successfully";
    }



    @RequestMapping(value="/report",method = RequestMethod.GET)
    public List<Report> generateReport(@RequestParam(value="date", required=false, defaultValue="29/03/2018") String date
                                    ) {

        System.out.println(date);

        List<Report> reports=inventoryService.generateReport(date);
        return reports;
    }
}
