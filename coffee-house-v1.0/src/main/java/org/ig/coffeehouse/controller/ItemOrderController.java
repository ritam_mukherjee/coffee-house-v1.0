package org.ig.coffeehouse.controller;

import org.ig.coffeehouse.model.Coffee;
import org.ig.coffeehouse.model.ItemOrder;
import org.ig.coffeehouse.services.InventoryService;
import org.ig.coffeehouse.services.ItemOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
public class ItemOrderController {

    @Autowired
    private ItemOrderService itemOrderService;

    @RequestMapping("/itemOrders")
    public List<ItemOrder> getAllCoffees() {

        DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("DATE:>"+dateFormat.format(new Date()));
        itemOrderService.addItemOrder(new ItemOrder("I11","C11","C11",dateFormat.format(new Date())));

        return itemOrderService.getAllItemOrders();
    }


    @RequestMapping(value="/addItemOrder",method = RequestMethod.POST)
    public ModelAndView addCustomer(@RequestBody List<ItemOrder>itemOrders) {

        System.out.println("BEFORE:"+itemOrders.size());

        itemOrders.forEach(itemOrder -> itemOrderService.addItemOrder(itemOrder));
        System.out.println("AFTER:"+itemOrders.size());
        return new ModelAndView(new RedirectView("forward:customers",true));
    }



}
