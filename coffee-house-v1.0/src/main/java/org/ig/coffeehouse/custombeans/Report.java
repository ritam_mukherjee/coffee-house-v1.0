package org.ig.coffeehouse.custombeans;

public class Report {

    private String coffee_id;
    private String ccoffee_type;
    private int initial_availability;
    private int selling_availability;

    public String getCoffee_id() {
        return coffee_id;
    }

    public void setCoffee_id(String coffee_id) {
        this.coffee_id = coffee_id;
    }

    public String getCcoffee_type() {
        return ccoffee_type;
    }

    public void setCcoffee_type(String ccoffee_type) {
        this.ccoffee_type = ccoffee_type;
    }

    public int getInitial_availability() {
        return initial_availability;
    }

    public void setInitial_availability(int initial_availability) {
        this.initial_availability = initial_availability;
    }

    public int getSelling_availability() {
        return selling_availability;
    }

    public void setSelling_availability(int selling_availability) {
        this.selling_availability = selling_availability;
    }
}
