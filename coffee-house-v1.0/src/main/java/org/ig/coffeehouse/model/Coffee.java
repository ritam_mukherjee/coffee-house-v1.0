package org.ig.coffeehouse.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class Coffee {
    @Id
    private String coffee_id;
    private String coffee_type;
    private String coffee_description;
    private int available_serving;

    public Coffee() {
    }

    public Coffee(String coffee_id, String coffee_type, String coffee_description, int available_serving) {
        this.coffee_id = coffee_id;
        this.coffee_type = coffee_type;
        this.coffee_description = coffee_description;
        this.available_serving = available_serving;
    }

    @Override
    public String toString() {
        return "Coffee{" +
                "coffee_id='" + coffee_id + '\'' +
                ", coffee_type='" + coffee_type + '\'' +
                ", coffee_description='" + coffee_description + '\'' +
                ", available_serving=" + available_serving +
                '}';
    }

    public String getCoffee_id() {
        return coffee_id;
    }

    public void setCoffee_id(String coffee_id) {
        this.coffee_id = coffee_id;
    }

    public String getCoffee_type() {
        return coffee_type;
    }

    public void setCoffee_type(String coffee_type) {
        this.coffee_type = coffee_type;
    }

    public String getCoffee_description() {
        return coffee_description;
    }

    public void setCoffee_description(String coffee_description) {
        this.coffee_description = coffee_description;
    }

    public int getAvailable_serving() {
        return available_serving;
    }

    public void setAvailable_serving(int available_serving) {
        this.available_serving = available_serving;
    }
}
