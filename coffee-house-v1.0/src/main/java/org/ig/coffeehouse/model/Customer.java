package org.ig.coffeehouse.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Customer {
    @Id
    private String customerId;
    private int phno;
    private String name;


    @Override
    public String toString() {
        return "Customer{" +
                "customerId='" + customerId + '\'' +
                ", phno=" + phno +
                ", name='" + name + '\'' +
                '}';
    }


    public Customer() {
    }

    public Customer(String customerId, int phno, String name) {
        this.customerId = customerId;
        this.phno = phno;
        this.name = name;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getPhno() {
        return phno;
    }

    public void setPhno(int phno) {
        this.phno = phno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
