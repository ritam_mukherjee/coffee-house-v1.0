package org.ig.coffeehouse.model;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Inventory {
    @Id
    private String inventoryId;
    private String coffee_id;
    private int initial_availability;
    private int selling_availability;
    private String date;

    public Inventory() {
    }

    public Inventory(String inventoryId, String coffee_id, int initial_availability, int selling_availability, String date) {
        this.inventoryId = inventoryId;
        this.coffee_id = coffee_id;
        this.initial_availability = initial_availability;
        this.selling_availability = selling_availability;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "inventoryId='" + inventoryId + '\'' +
                ", coffee_id='" + coffee_id + '\'' +
                ", initial_availability=" + initial_availability +
                ", selling_availability=" + selling_availability +
                ", date=" + date +
                '}';
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getCoffee_id() {
        return coffee_id;
    }

    public void setCoffee_id(String coffee_id) {
        this.coffee_id = coffee_id;
    }

    public int getInitial_availability() {
        return initial_availability;
    }

    public void setInitial_availability(int initial_availability) {
        this.initial_availability = initial_availability;
    }

    public int getSelling_availability() {
        return selling_availability;
    }

    public void setSelling_availability(int selling_availability) {
        this.selling_availability = selling_availability;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
