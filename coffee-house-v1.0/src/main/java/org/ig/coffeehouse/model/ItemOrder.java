package org.ig.coffeehouse.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ItemOrder {

    @Id
    private String item_order_id;
    private String customerId;
    private String coffee_id;
    private String date;

    public ItemOrder() {
    }

    public ItemOrder(String item_order_id, String customerId, String coffee_id, String date) {
        this.item_order_id = item_order_id;
        this.customerId = customerId;
        this.coffee_id = coffee_id;
        this.date = date;
    }

    @Override
    public String toString() {
        return "ItemOrder{" +
                "item_order_id='" + item_order_id + '\'' +
                ", customerId='" + customerId + '\'' +
                ", coffee_id='" + coffee_id + '\'' +
                ", date='" + date + '\'' +
                '}';
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public void setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCoffee_id() {
        return coffee_id;
    }

    public void setCoffee_id(String coffee_id) {
        this.coffee_id = coffee_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
