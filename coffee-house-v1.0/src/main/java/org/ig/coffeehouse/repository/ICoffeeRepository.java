package org.ig.coffeehouse.repository;

import org.ig.coffeehouse.model.Coffee;
import org.springframework.data.repository.CrudRepository;

public interface ICoffeeRepository extends CrudRepository<Coffee,String> {
}
