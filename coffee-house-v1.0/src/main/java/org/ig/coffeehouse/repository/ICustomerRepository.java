package org.ig.coffeehouse.repository;

import org.ig.coffeehouse.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface ICustomerRepository extends CrudRepository<Customer,String>{
}
