package org.ig.coffeehouse.repository;

import org.ig.coffeehouse.model.Inventory;
import org.springframework.data.repository.CrudRepository;

public interface IInventoryRepository extends CrudRepository<Inventory,String> {

}
