package org.ig.coffeehouse.repository;

import org.ig.coffeehouse.model.ItemOrder;
import org.springframework.data.repository.CrudRepository;

public interface IItemOrderRepository extends CrudRepository<ItemOrder,String> {
}
