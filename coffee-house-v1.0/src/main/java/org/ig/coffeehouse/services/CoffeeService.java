package org.ig.coffeehouse.services;

import org.ig.coffeehouse.model.Coffee;
import org.ig.coffeehouse.repository.ICoffeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CoffeeService implements ICoffeeService{

    @Autowired
    private ICoffeeRepository coffeeRepository;

    @Override
    public List<Coffee> getAllCoffees(){

        List<Coffee> coffees=new ArrayList<>();
        coffeeRepository.findAll().forEach(coffees::add);
        return coffees;
    }

    @Override
    public String getCoffee(String coffee_id){
        Coffee coffee=getAllCoffees().stream()
                .filter(c -> c.getCoffee_id().equalsIgnoreCase(coffee_id)).findFirst().get();


     return coffee==null?"":coffee.getCoffee_type();
    }

    @Override
    public void addCoffee(Coffee coffee){
        coffeeRepository.save(coffee);
    }

    @Override
    public void updateSellingAvailability(String coffee_id,int servs){

        System.out.println("Size of Repository "+coffeeRepository.count());
        for (Coffee coffee:coffeeRepository.findAll()
                ) {
            if(coffee_id.equalsIgnoreCase(coffee.getCoffee_id())){
                coffee.setAvailable_serving(coffee.getAvailable_serving()-servs);
                coffeeRepository.save(coffee);
                return;

            }
        }
    }
}
