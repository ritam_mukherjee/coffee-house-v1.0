package org.ig.coffeehouse.services;


import org.ig.coffeehouse.model.Customer;
import org.ig.coffeehouse.repository.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService implements ICustomerService {

    @Autowired
    private ICustomerRepository customerRepo;

  /*  private List<Customer> customers=new ArrayList<>(Arrays.asList(
            new Customer("aa",22,"amar"),
            new Customer("bb",33,"hamar")
    ));*/

    @Override
    public List<Customer> getAllCustomers() {

        List<Customer> customers = new ArrayList<>();
        customerRepo.findAll().forEach(customers::add);
        return customers;
    }

    @Override
    public void addCustomer(Customer customer) {
        customerRepo.save(customer);
    }
}
