package org.ig.coffeehouse.services;

import org.ig.coffeehouse.model.Coffee;

import java.util.List;

public interface ICoffeeService {
    List<Coffee> getAllCoffees();

    String getCoffee(String coffee_id);

    void addCoffee(Coffee coffee);

    void updateSellingAvailability(String coffee_id, int servs);
}
