package org.ig.coffeehouse.services;

import org.ig.coffeehouse.model.Customer;

import java.util.List;

public interface ICustomerService {
    List<Customer> getAllCustomers();

    void addCustomer(Customer customer);
}
