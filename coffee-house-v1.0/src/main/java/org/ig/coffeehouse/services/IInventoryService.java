package org.ig.coffeehouse.services;

import org.ig.coffeehouse.custombeans.Report;
import org.ig.coffeehouse.model.Inventory;

import java.util.List;

public interface IInventoryService {
    List<Inventory> getAllInventoris();

    void addInventory(Inventory inventory);

    /*do update based on Coffee_ID*/
    void updateSellingAvailability(String inventoryId, int servs);

    /*Do update based on cofee_ID*/
    void updateSellingAvailability_cofffe_Id(String coffee_id, int servs);

    List<Report> generateReport(String date);
}
