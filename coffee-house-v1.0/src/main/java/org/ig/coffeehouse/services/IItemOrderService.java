package org.ig.coffeehouse.services;

import org.ig.coffeehouse.model.ItemOrder;

import java.util.List;

public interface IItemOrderService {
    List<ItemOrder> getAllItemOrders();

    void addItemOrder(ItemOrder itemOrder);
}
