package org.ig.coffeehouse.services;

import org.ig.coffeehouse.custombeans.Report;
import org.ig.coffeehouse.model.Coffee;
import org.ig.coffeehouse.model.Inventory;
import org.ig.coffeehouse.repository.IInventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryService implements IInventoryService {

    @Autowired
    private IInventoryRepository inventoryRepository;

    @Autowired
    private CoffeeService coffeeService;


    @Override
    public List<Inventory> getAllInventoris() {

        List<Inventory> inventories = new ArrayList<>();
        inventoryRepository.findAll().forEach(inventories::add);
        return inventories;
    }

    @Override
    public void addInventory(Inventory inventory) {
        inventoryRepository.save(inventory);
    }


    /*do update based on Coffee_ID*/
    @Override
    public void updateSellingAvailability(String inventoryId, int servs) {

        System.out.println("Size of Repository " + inventoryRepository.count());
        for (Inventory inventory : inventoryRepository.findAll()
                ) {
            if (inventoryId.equalsIgnoreCase(inventory.getInventoryId())) {
                inventory.setSelling_availability(inventory.getSelling_availability() - servs);
                inventoryRepository.save(inventory);
                return;

            }
        }
    }


    /*Do update based on cofee_ID*/
    @Override
    public void updateSellingAvailability_cofffe_Id(String coffee_id, int servs) {

        System.out.println("Size of Repository " + inventoryRepository.count());
        for (Inventory inventory : inventoryRepository.findAll()
                ) {
            if (coffee_id.equalsIgnoreCase(inventory.getCoffee_id())) {
                inventory.setSelling_availability(inventory.getSelling_availability() - servs);
                inventoryRepository.save(inventory);
                return;

            }
        }
    }

    @Override
    public List<Report> generateReport(String date) {

        List<Inventory> inventories = getAllInventoris();
        List<Report> reports = new ArrayList<>();

        inventories.stream().filter(inventory -> inventory.getDate().equalsIgnoreCase(date))
                .forEach(inventory -> {
                    Report report = new Report();

                    Coffee coffee = coffeeService.getAllCoffees().stream()
                            .filter(c -> c.getCoffee_id()
                                    .equalsIgnoreCase(inventory.getCoffee_id()))
                            .findFirst().get();
                    report.setCcoffee_type(coffee == null ? "" : coffee.getCoffee_type());
                    report.setCoffee_id(inventory.getCoffee_id());

                    report.setInitial_availability(inventory.getInitial_availability());
                    report.setSelling_availability(inventory.getSelling_availability());
                    reports.add(report);
                });

        System.out.println("report Size" + reports.size());

        return reports;
    }
}
