package org.ig.coffeehouse.services;

import org.ig.coffeehouse.model.ItemOrder;
import org.ig.coffeehouse.repository.IItemOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemOrderService implements IItemOrderService {

    @Autowired
    private IItemOrderRepository itemOrderRepository;


    @Override
    public List<ItemOrder> getAllItemOrders(){

        List<ItemOrder> items_orders=new ArrayList<>();
        itemOrderRepository.findAll().forEach(items_orders::add);
        return items_orders;
    }
    @Override
    public void addItemOrder(ItemOrder itemOrder){
        itemOrderRepository.save(itemOrder);
    }
}
