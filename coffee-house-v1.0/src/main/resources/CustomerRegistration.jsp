<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Add Employee</title>
</head>
<body>
<div id="addEmployee">
    <form:form action="addNewCustomer.do" method="post">
        <p>
            <label>Enter Customer Id<fmt:message key="customer.customerId" /></label>
            <form:input path="customerId" />
        </p>
        <p>
            <label>Enter Customer Ph No<fmt:message key="customer.phno" /></label>
            <form:input path="phno" />
        </p>
        <p>
            <label>Enter Name<fmt:message key="customer.name" /></label>
            <form:input path="name" />
        </p>
        <input type="submit" value="Add New Employee" />
    </form:form>
</div>
</body>
</html>