package org.ig.coffeehouse.test;


import org.ig.coffeehouse.controller.CoffeeController;
import org.ig.coffeehouse.controller.InventoryController;
import org.ig.coffeehouse.model.Coffee;
import org.ig.coffeehouse.repository.ICoffeeRepository;
import org.ig.coffeehouse.services.CoffeeService;
import org.ig.coffeehouse.services.CustomerService;
import org.ig.coffeehouse.services.InventoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static javax.ws.rs.core.Response.*;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(MockitoJUnitRunner.class)
public class TestCoffeeService {



    @Autowired
    private WebApplicationContext  webApplicationContext;

    @Mock
    private CoffeeService coffeeService;

    @Mock
    private InventoryService inventoryService;

    @InjectMocks
    private CoffeeController coffeeController;

    @InjectMocks
    private InventoryController inventoryController;

    private MockMvc mockMvc;


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mockMvc= MockMvcBuilders.standaloneSetup(coffeeController).build();
    }

    /**
     * Testing Retrieve Functionality
     */
    @Test
    public void getAllCoffesTest(){
        List<Coffee> coffees=new ArrayList<>(Arrays.asList(
                new Coffee("C11", "Cappucino", "Lite", 50),
                new Coffee("C22", "Latte", "Strong", 100)));

        when(coffeeService.getAllCoffees()).thenReturn((List)coffees);

        try {
            mockMvc.perform(get("/coffees")).andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Testiing of CoffeOrder functionality
     * {Update functionality of backend}
     */
    @Test
    public void addCoffee() {

        try {
            mockMvc.perform(put("/coffeeOrder")).andExpect(status().isOk());
           /* when(coffeeService.getAllCoffees()).thenReturn((List) coffees);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
